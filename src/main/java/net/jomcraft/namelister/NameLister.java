package net.jomcraft.namelister;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class NameLister {
	
    public static void main(String[] args) {

		File file = new File("lang.txt");
		if(!file.exists())
			return;
		
		try (BufferedReader reader = new BufferedReader(new FileReader(file)); PrintWriter writer = new PrintWriter(new FileWriter(new File("newLang.txt")))){
			String line;
			int id = 1;
			while ((line = reader.readLine()) != null) {
				writer.print("name.surname" + id + "=" + line + "\n");
				id++;
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
    
}
